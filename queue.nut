class Queue {

    _queue = null

	constructor() {
		this._queue = []
	}

    // Return true if the _queue is empty
    function empty() {
        return this._queue.len() == 0
    }
    
    // Return the number of elements in the _queue
    function size() {
        return this._queue.len()
    }
    
    // Return the front element of the _queue without removing it
    function front() {
        if (!this._queue.len()) {
            return null
        }
        return this._queue[0]
    }
    
    // Return the back element of the _queue without removing it
    function back() {
        if (!this._queue.len()) {
            return null
        }
        return this._queue[this._queue.len() - 1]
    }
    
    // Add an element to the back of the _queue
    function push(element) {
        this._queue.append(element)
    }
    
    // Remove and return the front element of the _queue
    function pop() {
        if (this.empty()) {
            return null
        }
        return this._queue.remove(0)
    }
}

dofile("core_utils.nut")
pprint <- CoreUtils.pprint

/*
local q = Queue()
local r = Queue()
q.push(1)
q.push(2)
q.push(3)
pprint(q.empty())
pprint(q.size())
pprint(q.front())
pprint(q.back())
pprint(q.pop())
pprint(q.pop())
pprint(q.pop())
pprint(q.empty())
*/
