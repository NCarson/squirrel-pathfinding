dofile("core_utils.nut")
dofile("priority_queue.nut")
dofile("graph.nut")
dofile("base_search.nut")


//https://www.redblobgames.com/pathfinding/a-star/implementation.html

class DijkstraSearch extends BaseSearch {

	function search(graph, start, goal) {
		this._iterations = 0
		// This actually does not overflow on higher numbers but I guess its big enough
		local INF = pow(2, (8*_intsize_)-1) 
		local frontier = PriorityQueue()	
		local came_from = {}
		local cost_so_far = {}
		frontier.push(start, 0)
		came_from[start] <- null
		cost_so_far[start] <- 0

		while (!frontier.empty()) {
			local current = frontier.pop()
			if (current == goal)
				break

			//print(graph.neighbors(current, exclude).len())
			foreach (next in graph.neighbors(current)) {
				this._iterations++
				local cost = graph.cost(current, next)
				local new_cost = cost_so_far[current] + cost
				//if (new_cost < Table.get(cost_so_far, next, INF)) {
				if (!Table.haskey(cost_so_far, next)
					|| new_cost < cost_so_far[next]
					) {

					cost_so_far[next] <- new_cost
					frontier.push(next, new_cost)
					//frontier.push(next, 1)
					came_from[next] <- current
				}
			}
		}
		return came_from
	}
}



