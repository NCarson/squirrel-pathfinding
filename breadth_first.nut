dofile("queue.nut")
dofile("graph.nut")
dofile("base_search.nut")

//https://www.redblobgames.com/pathfinding/a-star/implementation.html
class BreadthFirstSearch extends BaseSearch {

	function search(graph, start, goal=null) {
		this._iterations = 0
		local frontier = Queue()	
		frontier.push(start)
		local came_from = {}
		came_from[start] <- null

		while (!frontier.empty()) {
			local current = frontier.pop()
			if (current == goal) // we may not have goal
				break

			foreach (next in graph.neighbors(current)) {
				this._iterations++
				local visited = Table.keys(came_from)
				if (!Array.contains(visited, next)) {
					frontier.push(next)
					came_from[next] <- current
				}
			}
		}
		return came_from
	}
}

