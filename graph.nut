dofile("core_utils.nut")
pprint <- CoreUtils.pprint
Array <- CoreUtils.Array
Table <- CoreUtils.Table

class Graph {

	_max_x = null
	_max_y = null
	_map = null

	symbols = null
	E = 1
	W = -1
	N = null
	S = null
	DIR_NONE = null

	SYM_W = "←"
	SYM_E = "→"
	SYM_N = "↑"
	SYM_S = "↓"
	SYM_NONE = "†"

	constructor(max_x, max_y, symbols=null) {
		this._max_x = max_x
		this._max_y = max_y
		this.symbols = symbols
		this.N = -max_x
		this.S = max_x
		this.clear()
	}

	function _tostring() {
		local out = ""
		local lens = []
		for (local i=0; i<this._max_y*this._max_x; i++) {
			lens.append(this._map[i].tostring().len())
		}
		local biggest = Array.max(lens)

		for (local i=0; i<this._max_y*this._max_x; i++) {
			if (i && (!(i % this._max_x)))
				out += "\n"
			local s = this._map[i].tostring()
			local n = s.len()
			if (n < biggest) {
				for (local i=biggest-n; i>0; i--) // add lpad
					out += " "
				out += s
			} else
				out += s
			out += " "
		}
		return out
	}

	function clear() {
		this._map = []
		this._map.resize(this._max_x * this._max_y, 0)
	}

	function get(i) {
		if (i >= (this._max_x*this._max_y) || i < 0)
			throw a[0] + " is out of bounds for i"
		return this._map[i]
	}

	function set(i, val) {
		if (i >= (this._max_x*this._max_y) || i < 0)
			throw i + " is out of bounds for i"
		this._map[i] = val
	}

	function iterIndexes() {
		for (local i=0; i<this._max_x*this._max_y; i++) {
			yield i
		}
	}

	function indexes() {
		local out = []
		foreach (i in this.iterIndexes())
			out.append(i)
		return out
	}

	function find(val) {
		local out = []
		foreach (i in this.iterIndexes()) {
			if (this._map[i] == val)
				out.append(i)
		}
		return out
	}

	function neighbors(idx, exclude=[]){
		if (idx >= this._max_x*this._max_y || idx < 0)
			return []
		if (Array.contains(exclude, this._map[idx]))
			return []

		// we have to add more guards if we add the diagnals for wrap around
		local dirs = [this.N, this.E, this.S, this.W]
		local out = []
		local i 
		foreach (dir in dirs) {
			i = idx + dir
			if (i >= this._max_x*this._max_y || i < 0) // out of bonds
				continue
			if ((dir == this.E) && i % this._max_x == 0) // wraps east
				continue
			if (dir == this.W && ((i % this._max_x) == this._max_x -1)) // wraps west
				continue
			if (Array.contains(exclude, this._map[i])) // blocked nodes
				continue
			out.append(i)
		}
		return out
	}

	function indexToInteger(idx) {
		return idx[1] * this._max_x + idx[0]
	}

	function integerToIndex(i) {
		return [i % this._max_x, i / this._max_x]
	}

	function setFromString(str) {
		if (this.symbols == null)
			throw "this.symbols is not set"

		local values = Table.keys(this.symbols)
		local i = 0
		values.push('\n')
		foreach(char in str) {
			if (!Array.contains(values, char))
				throw "unknown '" + char.tochar() + "' in string"
			if (char == '\n') {
				continue
			}
			local val = this.symbols[char]
			this._map[i] = val
			i += 1
		}
	}

	function getString() {
		if (this.symbols == null)
			throw "this.symbols is not set"

		local newkeys = {}
		local out = ""
		foreach (key, val in this.symbols) { // reverse the table
			newkeys[val] <- key.tochar()
		}
		foreach (i in this.iterIndexes()) {
			local val = this._map[i]
			if (i  && !(i % this._max_x)) {
				out += "\n"
			}
			out += Table.get(newkeys, val, val) //XXX should we allow this?
		}
		return out
	}

	function cost(current, next) {
		return this.get(next)
	}

	function heuristic(current, next) {
		// manhattan
		local idx1 = this.integerToIndex(current)
		local idx2 = this.integerToIndex(next)
		return abs(idx2[0] - idx1[0]) + abs(idx2[1] - idx1[1])
	}

	function getDirecSymbol(direc) {
		switch(direc) {
			case this.N: return this.SYM_N;
			case this.S: return this.SYM_S;
			case this.W: return this.SYM_W;
			case this.E: return this.SYM_E;
			case this.DIR_NONE: return this.SYM_NONE;
			default: return "?"
		}
	}
}

/* FIXME throws error "trying to set class"
class SymbolGraph extends Graph {

	symbols = null

	constructor(max_x, max_y, symbols) {
		Graph.constructor(max_x, max_y)
		this.symbols = symbols
	}

}
*/


