dofile("core_utils.nut")
pprint <- CoreUtils.pprint
Array <- CoreUtils.Array

class PriorityQueue {
	
	_count = null
	_queue = null

	constructor() {
		this.clear()
	}

	function clear() {
		this._queue= []
		this._count = 0
	}
	function empty() { return this._count == 0 }
	function size() { return this._count}

	function push(data, priority) {

		/* Append dummy entry */
		this._queue.append(0);
		this._count++;

		local hole;
		/* Find the point of insertion */
		for (hole = _count - 1; hole > 0 && priority <= this._queue[hole / 2][1]; hole /= 2)
			this._queue[hole] = this._queue[hole / 2];
		/* Insert new pair */
		this._queue[hole] = [data, priority];
	}

	function pop(){
		if (_count == 0) return null;

		local node = this._queue[0];
		/* Remove the item from the list by putting the last value on top */
		this._queue[0] = this._queue[_count - 1];
		this._queue.pop();
		this._count--;
		/* Bubble down the last value to correct the tree again */
		this._heapify();
		return node[0];
	}

	function _heapify() {

		if (_count == 0) return;

		local hole = 1;
		local tmp = _queue[0];

		/* Start switching parent and child until the tree is restored */
		while (hole * 2 < _count + 1) {
			local child = hole * 2;
			if (child != _count && _queue[child][1] <= _queue[child - 1][1]) child++;
			if (_queue[child - 1][1] > tmp[1]) break;

			_queue[hole - 1] = _queue[child - 1];
			hole = child;
		}
		/* The top value is now at his new place */
		_queue[hole - 1] = tmp;
	}

	function _validate() {
		local test = PriorityQueue()
		test._queue = Array.copy(this._queue)
		test._count = this._count
		local a = []
		while(!test.empty())
			a.append(test.pop())
		local b = Array.copy(a)
		b.sort()
		return Array.equals(a, b)
	}
}

/*
local queue = PriorityQueue()
local data = 0
queue.push(3, 3)
queue.push(4, 4)
queue.push(9, 9)
queue.push(2, 2)
queue.push(2, 2)
queue.push(5, 5)
queue.push(0, 0)
pprint(queue._validate())
*/
