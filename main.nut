dofile("dijkstra.nut")
dofile("breadth_first.nut")
dofile("astar.nut")
dofile("graph.nut")

function setPathSteps(graph, search, came_from, start=null, goal=null) {

	foreach (idx in graph.iterIndexes()) {
		local path = search.retraceSteps(came_from, start, idx)
		if (path.len())
			graph.set(idx, path.len()-1)
		else
			graph.set(idx, "X")

		if (start != null && idx == start)
			graph.set(idx, "S")
		if (goal != null && idx == goal)
			graph.set(idx, "G")
	}
}

function setPathDirecs(graph, search, came_from, start=null, goal=null) {

	foreach (idx in graph.iterIndexes()) {
		local path = search.retraceSteps(came_from, start, idx)
		path.reverse()
		if (path.len() > 1 && path[1] != null) {
			local direc = path[1] - path[0]
			graph.set(idx, graph.getDirecSymbol(direc))
		} else
			graph.set(idx, graph.getDirecSymbol(graph.DIR_NONE))

		if (start != null && idx == start)
			graph.set(idx, "S")
		if (goal != null && idx == goal)
			graph.set(idx, "G")
	}
}

function setPathGoal(graph, search, came_from, start, goal) {

	local path = search.retraceSteps(came_from, start, goal)
	foreach (idx in path) {
		graph.set(idx, "1")
		if (start != null && idx == start)
			graph.set(idx, "S")
		if (goal != null && idx == goal)
			graph.set(idx, "G")
	}
}


local key = {}
key['@'] <- 20
key['.'] <- 1
key['X'] <- 9
local map = Graph(16,8,key)
local maze = @"
................
.......@........
......@.@.......
.......@@.......
.......@@.......
.........@......
................
................"
map.setFromString(maze.slice(1))
pprint(map)
pprint("\n")
pprint(map.getString())

local start = map.indexToInteger([1,4])
local goal = map.indexToInteger([13,3])
local neighbo

//local search = BreadthFirstSearch(); goal = null
//local search = DijkstraSearch()
//local search = AStarSearch()

local came_from = search.search(map, start, goal)
local path = search.retraceSteps(came_from, start, goal)

local map = Graph(16,8)
setPathGoal(map, search, came_from, start, goal)
pprint(map)

local map = Graph(16,8)
setPathSteps(map, search, came_from, start, goal)
pprint(map)

local map = Graph(16,8)
setPathDirecs(map, search, came_from)
pprint(map)

pprint(search.iterations())


