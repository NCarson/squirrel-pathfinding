dofile("core_utils.nut")
dofile("priority_queue.nut")
dofile("base_search.nut")


//https://www.redblobgames.com/pathfinding/a-star/implementation.html

class AStarSearch extends BaseSearch {

	function search(graph, start, goal) {

		this._iterations = 0
		local frontier = PriorityQueue()	
		local came_from = {}
		local cost_so_far = {}
		frontier.push(start, 0)
		came_from[start] <- null
		cost_so_far[start] <- 0

		while (!frontier.empty()) {
			local current = frontier.pop()
			if (current == goal)
				break

			foreach (next in graph.neighbors(current)) {
				this._iterations++
				local cost = graph.cost(current, next)
				local new_cost = cost_so_far[current] + cost
				if (!Table.haskey(cost_so_far, next)
					|| new_cost < cost_so_far[next]
					) {

					cost_so_far[next] <- new_cost + graph.heuristic(next, goal)
					frontier.push(next, new_cost)
					//frontier.push(next, 1)
					came_from[next] <- current
				}
			}
		}
		return came_from
	}
}



