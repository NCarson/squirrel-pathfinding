#INSTALL_DIR = ~/.local/share/openttd/game/library/util
#SRC = $(wildcard *.nut ) lang readme.txt license.txt #changelog.txt
#REGISTRY_FILE = library.nut
#API_FILE = api.mkd

# my own git commands
# the beginning dash makes it optional
GITLAB_USER = NCarson
GITLAB_PROJECT = squirrel-pathfinding
-include ~/Makefile.git

# files to package scripts for openttd
include Makefile.openttd
