dofile("core_utils.nut")
Table <- CoreUtils.Table
Set <- CoreUtils.Set
Array <- CoreUtils.Array

class BaseSearch {
	
	// This actually does not overflow on higher numbers but I guess its big enough
	static INF = pow(2, (8*_intsize_)-1) 
	static MAX_ITER = 10000
	_iterations = null

	function iterations() { return this._iterations} 

	function retraceSteps(came_from, start, goal) {
		if (!Table.haskey(came_from, goal))
			return []
		local current = goal
		local out = []
		local i = 0
		while (current != start) {
			if (i > this.MAX_ITER)
				throw "too many iterations in loop; breaking out"
			out.append(current)
			current = came_from[current]
			i++
		}
		out.append(start)
		out.reverse()
		return out
	}
}
